package ru.anenkov.tm.repository;

import ru.anenkov.tm.api.repository.IProjectRepository;
import ru.anenkov.tm.entity.User;
import ru.anenkov.tm.exception.empty.EmptyIdException;
import ru.anenkov.tm.exception.system.IncorrectIndexException;
import ru.anenkov.tm.exception.empty.EmptyNameException;
import ru.anenkov.tm.entity.Project;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ProjectRepository implements IProjectRepository {

    private final List<Project> projects = new ArrayList<>();

    @Override
    public void merge(final Collection<Project> projects) {
        if (projects == null) return;
        for (final Project project : projects) merge(project);
    }

    @Override
    public Project merge(final Project project) {
        if (project == null) return null;
        projects.add(project);
        return project;
    }

    @Override
    public void merge(final Project... projects) {
        if (projects == null) return;
        for (final Project project : projects) merge(project);
    }

    @Override
    public void load(final Collection<Project> projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(final Project... projects) {
        clear();
        merge(projects);
    }

    @Override
    public void load(final Project project) {
        clear();
        merge(project);
    }

    @Override
    public void clear() {
        projects.clear();
    }

    @Override
    public List<Project> getListProjects() {
        return projects;
    }

    @Override
    public void add(final String userId, final Project project) {
        project.setUserId(userId);
        if (project == null) return;
        projects.add(project);
    }

    @Override
    public void remove(final String userId, final Project project) {
        if (!userId.equals(project.getUserId())) return;
        if (project == null) return;
        this.projects.remove(project);
    }

    @Override
    public List<Project> findAll(final String userId) {
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result;
    }

    @Override
    public void clear(final String userId) {
        final List<Project> projects = findAll(userId);
        this.projects.removeAll(projects);
    }

    @Override
    public Project findOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        return result.get(index);
    }

    @Override
    public Project findOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        for (final Project project : result) {
            if (name.equals(project.getName())) return project;
        }
        return null;
    }

    @Override
    public Project findOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final List<Project> result = new ArrayList<>();
        for (final Project project : projects) {
            if (userId.equals(project.getUserId())) result.add(project);
        }
        for (final Project project : result) {
            if (id.equals(project.getId())) return project;
        }
        return null;
    }

    @Override
    public Project removeOneByIndex(final String userId, final Integer index) {
        if (index == null || index < 0) throw new IncorrectIndexException();
        final Project project = findOneByIndex(userId, index);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneById(final String userId, final String id) {
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        final Project project = findOneById(userId, id);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

    @Override
    public Project removeOneByName(final String userId, final String name) {
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Project project = findOneByName(userId, name);
        if (project == null) return null;
        projects.remove(project);
        return project;
    }

}
