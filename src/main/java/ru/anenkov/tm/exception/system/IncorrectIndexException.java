package ru.anenkov.tm.exception.system;

import ru.anenkov.tm.exception.AbstractException;

public class IncorrectIndexException extends AbstractException {

    public IncorrectIndexException(Throwable cause) {
        super(cause);
    }

    public IncorrectIndexException(String value) {
        super("Error! This value ``" + value + "`` is not number... ");
    }

    public IncorrectIndexException() {
        super("Error! Index is incorrect!");
    }

}