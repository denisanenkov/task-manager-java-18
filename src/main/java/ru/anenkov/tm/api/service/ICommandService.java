package ru.anenkov.tm.api.service;

import ru.anenkov.tm.command.AbstractCommand;

import java.util.List;

public interface ICommandService {

    List<AbstractCommand> getCommandList();

}