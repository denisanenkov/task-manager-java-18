package ru.anenkov.tm.api.repository;

import ru.anenkov.tm.entity.Task;
import ru.anenkov.tm.entity.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(String email);

    User removeByLogin(String login);

    User removeUser(User user);

    User removeById(String id);

    User removeByEmail(String email);

    void clear();

    void merge(Collection<User> users);

    User merge(User user);

    void merge(User... users);

    void load(Collection<User> users);

    void load(User... users);

    void load(User user);

    List<User> getListUsers();

}
