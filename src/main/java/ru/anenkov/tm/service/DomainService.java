package ru.anenkov.tm.service;

import ru.anenkov.tm.api.service.*;
import ru.anenkov.tm.dto.Domain;

import java.io.Serializable;

public class DomainService implements IDomainService, Serializable {

    private final ITaskService taskService;

    private final IUserService userService;

    private final IProjectService projectService;

    public DomainService(ITaskService taskService, IUserService userService, IProjectService projectService) {
        this.taskService = taskService;
        this.userService = userService;
        this.projectService = projectService;
    }

    @Override
    public void load(final Domain domain) {
        if (domain == null) return;
        taskService.load(domain.getTasks());
        userService.load(domain.getUsers());
        projectService.load(domain.getProjects());
    }

    @Override
    public void export(final Domain domain) {
        if (domain == null) return;
        domain.setTasks(taskService.getTasksList());
        domain.setProjects(projectService.getProjectList());
        domain.setUsers(userService.getUserList());
    }

}
