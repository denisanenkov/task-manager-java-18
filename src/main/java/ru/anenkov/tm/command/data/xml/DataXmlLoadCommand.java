package ru.anenkov.tm.command.data.xml;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.constant.DataConst;
import ru.anenkov.tm.dto.Domain;
import ru.anenkov.tm.enumeration.Role;

import java.io.FileInputStream;

public class DataXmlLoadCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Data-xml-load";
    }

    @Override
    public String description() {
        return "Data XML load";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[DATA XML LOAD]");
        final String path = DataConst.FILE_XML;
        if (path == null) return;
        final FileInputStream fileInputStream = new FileInputStream(path);
        final ObjectMapper objectMapper = new XmlMapper();
        final Domain domain = objectMapper.readValue(fileInputStream, Domain.class);
        serviceLocator.getDomainService().load(domain);
        fileInputStream.close();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
