package ru.anenkov.tm.command.auth;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;

public class LogoutCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Logout";
    }

    @Override
    public String description() {
        return "Logout user";
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
