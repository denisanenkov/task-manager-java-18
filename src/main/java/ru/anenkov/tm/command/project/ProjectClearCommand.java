package ru.anenkov.tm.command.project;

import ru.anenkov.tm.command.AbstractCommand;
import ru.anenkov.tm.enumeration.Role;

public class ProjectClearCommand extends AbstractCommand {

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String name() {
        return "Project-clear";
    }

    @Override
    public String description() {
        return "Remove all projects";
    }

    @Override
    public void execute() {
        System.out.println("[CLEAR PROJECT]");
        final String userId = serviceLocator.getAuthService().getUserId();
        serviceLocator.getProjectService().clear(userId);
        System.out.println("[OK]");
    }

    @Override
    public Role[] roles() {
        return new Role[]{Role.USER, Role.ADMIN};
    }

}
